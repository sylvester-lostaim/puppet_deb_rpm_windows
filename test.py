#!/usr/bin/env python3

import curses,signal
from os import listdir
from os.path import expanduser
from math import floor
from curses.textpad import Textbox, rectangle

def update_screen(target_screen):
    # avoid screen flickering
    target_screen.noutrefresh()
    curses.doupdate()

def repaint_screen():
    global resize_screen
    main_screen.clear()
    main_interface()
    resize_screen=True
    # resize 1st split screen (non-current screen)
    if current_screen_display is True:
        if right_screen_display == False:
            resize_screen=False
            select_screen_display(left_screen_display)
        select_screen_display(right_screen_display)
    else:
        select_screen_display(left_screen_display)

def create_menu(options,target_screen):
    global current_screen_display,left_current_option,right_current_option,resize_screen
    # set current_option
    current_option=''
    if target_screen == left_screen:
        current_option = left_current_option
        current_screen_display = True
    else:
        current_option = right_current_option
        current_screen_display = False
    # if current list < previous list && previous option > current list
    # set current_option to last option
    if current_option >= len(options):
        current_option = len(options) - 1
    # set title bar
    color_bar_info()
    # clear previous screen
    target_screen.clear()
    while True:
        # list all options
        for count,item in enumerate(options):
            # only highlight current option
            if count == current_option:
                target_screen.addstr(count,0,item,curses.A_REVERSE)
            else:
                target_screen.addstr(count,0,item,curses.A_NORMAL)
        update_screen(target_screen)

        # resize 2nd split screen (current screen)
        if resize_screen is True:
            resize_screen=False
            if current_screen_display is True:
                select_screen_display(right_screen_display)
            else:
                select_screen_display(left_screen_display)

        # response to end user's signal
        signal=target_screen.getch()
        if signal == curses.KEY_RESIZE:
            repaint_screen()
        elif signal in [curses.KEY_ENTER, ord('\n')]:
            break
        elif signal == curses.KEY_LEFT:
            if current_screen_display == True:
                continue
            else:
                 select_screen_display(left_screen_display)
        elif signal == curses.KEY_RIGHT:
            if current_screen_display == False:
                continue
            else:
                if right_screen_display == False:
                    continue
                else:
                    select_screen_display(right_screen_display)
        elif signal == curses.KEY_UP:
            current_option = current_option - 1
            if current_option < 0:
                current_option = len(options) - 1
        elif signal == curses.KEY_DOWN:
            current_option=current_option+1
            if current_option >= len(options):
                current_option = 0
        elif signal == 23: # Ctrl-w
            exit_program(target_screen)
        if target_screen == left_screen:
            left_current_option=current_option
        else:
            right_current_option=current_option
    return current_option

def select_screen_display(display):
    if display == 1:
            interface_module_dir()
    elif display == 2:
            interface_module_list(global_module_list,global_module_dir_option)
    elif display == 3:
            interface_module_manifest_list(global_module_manifest_list)

def restrict_screen_size():
    global win_x, win_y, vertical_split_screen

    # screen size, get split screen value
    win_y,win_x=main_screen.getmaxyx()
    vertical_split_screen=int(win_x/2)

    # refuse small resolution size
    if win_x < 94 or win_y < 27:
        main_screen.clear()
        main_screen.addstr(0,0,"Resolution size is too small")
        update_screen(main_screen)
        while True:
            win_y, win_x = main_screen.getmaxyx()
            update_screen(main_screen)
            if win_x >= 94 and win_y >= 27:
                vertical_split_screen=int(win_x/2)
                main_screen.clear()
                break

def color_bar_info():
    for i in [left_top_bar,left_bottom_bar,right_top_bar,right_bottom_bar]:
        i.clear()
    left_style,right_style='',''
    if current_screen_display:
        left_style,right_style=curses.A_BOLD,curses.A_NORMAL
    else:
        left_style,right_style=curses.A_NORMAL,curses.A_BOLD
    # left_top
    left_top_bar.addstr(0,0,left_top_info,left_style)
    # left_bottom
    left_bottom_bar.addstr(0,0,left_bottom_info,left_style)
    # right_top
    right_top_bar.addstr(0,0,right_top_info,right_style)
    # right_bottom
    right_bottom_bar.addstr(0,0,right_bottom_info,right_style)
    for i in [left_top_bar,left_bottom_bar,right_top_bar,right_bottom_bar]:
        update_screen(i)

def exit_program(target_screen):
    for i in [left_bottom_bar,right_bottom_bar]:
        i.addstr(0,0,"Quit? (y/n)")
        update_screen(i)
    while True:
        if target_screen.getch() == 110:
            repaint_screen()
        elif target_screen.getch() == 121:
            curses.endwin()
            exit()

####
#### INTERFACES flow
#### initialization: main_interface
#### start: module_dir ~ module_list ~ module_manifest_list
####

def main_interface():
    global vertical_split_screen,left_screen,left_top_bar,left_bottom_bar,right_screen,right_top_bar,right_bottom_bar,win_x,win_y

    # detect screen size
    restrict_screen_size()

    # left split screen
    rectangle(main_screen,1,1,win_y-2,vertical_split_screen-1)
    left_screen=main_screen.subwin(win_y-4,vertical_split_screen-3,2,2)
    left_top_bar=main_screen.subwin(1,vertical_split_screen-3,0,2)
    left_bottom_bar=main_screen.subwin(1,vertical_split_screen-3,win_y-1,2)
    # right split screen
    rectangle(main_screen,1,vertical_split_screen+1,win_y-2,win_x-1)
    right_screen=main_screen.subwin(win_y-4,vertical_split_screen-3,2,vertical_split_screen+2)
    right_top_bar=main_screen.subwin(1,vertical_split_screen-3,0,vertical_split_screen+2)
    right_bottom_bar=main_screen.subwin(1,vertical_split_screen-3,win_y-1,vertical_split_screen+2)
    # top & bottom info bar
    update_screen(main_screen)

    # allow arrow movement
    left_screen.keypad(True)
    right_screen.keypad(True)

def interface_module_dir():
    global left_screen_display,left_top_info,left_bottom_info
    left_screen_display,left_top_info=1,"[ Module Directory Path ]"
    home_dir=expanduser("~")+"/.puppet/etc/code/modules"
    module_dir=("/etc/puppetlabs/code/environments/production/modules",
                "/etc/puppet/code/modules",
                home_dir,
                "/opt/puppetlabs/puppet/modules",
                "(Use other path...)")
    while True:
        # get user's option and process
        option=create_menu(module_dir,left_screen)
        if option == len(module_dir) - 1:
            print("Not yet implemented, welcome to BUG CITY")
        try:
            interface_module_list(listdir(module_dir[option]),module_dir[option])
        except (PermissionError, FileNotFoundError) as error:
            right_screen.clear()
            right_screen.addstr(0,0,"Path not found, or no access permission.\n("+module_dir[option]+")")
            update_screen(right_screen)

def interface_module_list(module_list,module_dir_option):
    global right_screen_display,global_module_list,global_module_dir_option,right_top_info
    right_screen_display,global_module_list,global_module_dir_option,right_top_info=2,module_list,module_dir_option,"[ Module List ]"
    option=create_menu(module_list,right_screen)
    interface_module_manifest_list(listdir(module_dir_option+"/"+module_list[option]+"/manifests"))

def interface_module_manifest_list(module_manifest_list):
    global right_screen_display,global_module_manifest_list,right_top_info
    right_screen_display,global_module_manifest_list,right_top_info=3,module_manifest_list,"[ Manifest Files (.pp) ]"
    option=create_menu(module_manifest_list,right_screen)


##########################################
# No __main__() like script, start here. #
##########################################

# ncurses initialization
main_screen = curses.initscr()
# no signal display
curses.noecho()
# no buffer input
curses.cbreak()
# no cursor
curses.curs_set(0)

# define screen variables
right_screen,right_top_bar,right_bottom_bar = '','',''
left_screen, left_top_bar, left_bottom_bar  = '','',''
resize_screen,vertical_split_screen,win_x,win_y = False,'',0,0

# initialize interface
main_interface()

# color bar info
left_top_info,left_bottom_info,right_top_info,right_bottom_info='','','',''

# initialize option
left_current_option,right_current_option = 0,0
global_module_list,global_module_dir_option,global_module_manifest_list='','',''

# initialize screen display
# current_screen_display: left/True, right/False
current_screen_display,left_screen_display,right_screen_display = False,False,False

# ignore SIGINT, SIGTERM
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.signal(signal.SIGTERM,signal.SIG_IGN)

# first main menu interface
interface_module_dir()




#box = Textbox(left_screen)
#box.edit()
#input = box.gather()
