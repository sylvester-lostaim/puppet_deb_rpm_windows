# puppet_deb_rpm_windows

Depends on: libncurses5

Update interface only, ctrl-w to exit.

Since, sigint and sigterm had been disabled. It is possible to cause terminal settings do not return to original state (if ncurses is not properly terminated), after force the pkill. Kindly, use "reset" command to refresh your terminal.
